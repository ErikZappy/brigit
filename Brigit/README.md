# Putting the rules for Brigit here... later
A brigit text file is calle a .tome and compiled brigit file
is called a .ctom

## Tags
### load:
exmpl:
> [load char:person1 person2; background: thingy;]
or
> [load char:person1 person2; background: thingy]

This tag must be set in the beginning of document. It tells the parser
and the scene what resources are required for the given DomTree.

### res:
exmpl:
> [res char:person1; background:thingy]
> Words fr dialog goes here
> [*]

## rep:

### r:

## Arguments within Tags

### char:
