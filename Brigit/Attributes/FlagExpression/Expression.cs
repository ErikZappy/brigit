﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brigit.Attributes.FlagExpression
{
    // this is top level and base level that will be seen by
    // the runtime
    public class Expression
    {
        // we iterate over this list
        // (a | b) & c
        // that for instance would iterate twice, one with an
        // or and a base.
        protected List<Expression> expressions;
        
        // for a singleton 
        private string variable;
        private bool singleton;
       

        // the operation that this expression will do
        // this is set by constructors of the inheriting classes
        protected char operation;
        

        // used for a non single expression
        public Expression()
        {
             
            expressions = new List<Expression>();
            singleton = false;
            variable = string.Empty;
        }

        // used for a single expression
        public Expression(string exp)
        {
            variable = exp;
            singleton = true;
            expressions = null;
        }

        public void Add(Expression exp)
        {
            if(!singleton)
            {
                expressions.Add(exp);
            }
            else
            {
                // can't happy so through an error?
                // TODO throw exception
            }
        }
        
        // this creates a new singelton with the exp and then adds that to the
        // list
        // this is the same thing that verita does actually
        public void Add(string exp)
        {
            Expression newExp = new Expression(exp);
            this.Add(newExp);
        }

        // if the expressions list is empty then this will become
        // a singleton
        // or it will error if the expression cannot be converted
        public void AddAndBecomeSingleton(string exp)
        {
            if (expressions.Count == 0)
            {
                singleton = true;
                variable = exp;
            }
            else
            {
                // throw an exception i guess
                // TODO throw and expcetion here?
            }
        }

        // an unset flag will override everything and make the whole expression
        // unset
        public virtual Flag Evaluate(Dictionary<string, Flag> localFlags)
        {
            // TODO this gets the flag from either local, or global and returns itn's current state
            if(singleton)
            {
                return localFlags[variable];
            }
            // something is horribly wrong if this is true
            // this is the base level object 
            // maybe i should have made these an interface
            else
            {
                return Flag.Unset;
            }
        }

        // TODO actually implement this
        public override bool Equals(object obj)
        {
            bool equals = false;

            if (!(obj is Expression))
            {
                return false;
            }

            Expression other = (Expression)obj;

            if((this.variable == other.variable) && expressions.Count == 0)
            {
                equals = true;
            }
            else if(expressions.Count == other.expressions.Count)
            {
                equals = true;
            }
            else
            {
                equals = false;
            }

            return equals;
        }
    }
}
