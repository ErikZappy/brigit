﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brigit.Attributes.FlagExpression
{
    public class And : Expression
    {
        public And() : base()
        {
            operation = '&';
        }

        public override Flag Evaluate(Dictionary<string, Flag> localFlags)
        {
            Flag eval = Flag.True;
            int i = 0;

            while(eval == Flag.True && i < expressions.Count)
            {
                Flag subEval = expressions[i].Evaluate(localFlags);
                eval = AndTwoFlags(eval, subEval);
                i++;
            }

            return eval;
        }

        public Flag AndTwoFlags(Flag f1, Flag f2)
        {
            if (f1 == Flag.False || f2 == Flag.False)
            {
                return Flag.False;
            }
            else if (f1 == Flag.Unset || f2 == Flag.Unset)
            {
                return Flag.Unset;
            }
            else
            {
                return Flag.True;
            }
        }
    }
}
