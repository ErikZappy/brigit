﻿This is where I'll brain storm some ideas and rules to set for the Brigit text files

* Documentation Start! *
>Tags<
load:
	exmpl: [load char: person1 person2; background: thingy;]
	the last semicolon is optional and the tag will parse just fine
	without it

	This tag must be set in the beginning of the document. It tells the
	parser to load the required resources for the scene. Resources such
	as characters, backgrounds and some other stuff that I might add later
	on. Characters, Backgrounds and basically anything must be loaded before
	being used or else the parser will throw an error.
res:
	empl:	[res char:person1 background: thingy]
				stuff goes here
			[*]
	A simple response node, IE one speech bubble of a conversation. takes a couple
	of arguments such as char. If char is not set for that response it will
	default to the char set by the res before it.
rep:
	empl: 	[rep char:person1]
				[r] stuff [*]
				[r] more stuff
					to choose from
				[*]
				[r]
					yurp[*]
			[*]
	A reply node. This requires user input to make a choice. Can take in char as
	an argument. Both RES and REP will take background as an argument as well.
r:
	A single reply. Must be used within the rep tag.

These are not tags but things that can changes tags or be set by tags
>Arguements<
char:
	Loads or uses char. Load will load the char resourse while the invocation
	of char in any other tag will set character is being the owner of that tag
	or speech/action.
background:
	Loads or uses a background resourse. It's the background not much to say about
	it. At least I don't think so.
emote:
	An emotion for a charater. It can change the portrait that is used for that
	character. If the character does not have that emote, then the default will
	be loaded.
flag: loc_flag, glo_flag
	Toggles a flag. There can be local flags, and global flags. These flags
	are set in the closing tags, while the opening tags will require the flags
	be set prior to be an availble path.

Fuck all that shit up there ^^ I changed the syntax and I think it's better because of it. I think

Character names exmpales:
	something
	Something
	Something Someone
	Some One
	Some_One
	Some 1
	Some_1
	some_1_one
	Some one some where	