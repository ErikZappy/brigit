﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Brigit.Attributes.FlagExpression;
using Brigit.Attributes;
using System.Collections.Generic;

namespace BrigitUnitTest
{
    [TestClass]
    public class UnitTest2
    {
        Dictionary<string, Flag> flags = new Dictionary<string, Flag>();
        string expressionToParse;
        string[] vars = new string[0];

        [TestMethod]
        public void TestNormalOrParsing()
        {
            // arrange
            expressionToParse = "thing|this";
            // act
            ExpressionParser.Preprocess(expressionToParse);
            Expression exp = ExpressionParser.Parse(expressionToParse, out vars);
            // assert
            // this should complete normally
        }

        public void TestFailingExpressionParsing()
        {
            // arrange
            expressionToParse = "this | that & you";
            // act
            ExpressionParser.Preprocess(expressionToParse);
            Expression exp = ExpressionParser.Parse(expressionToParse, out vars);
            // assert
            // this should error out
        }


    }
}
